# smartdenovo Singularity container
Bionformatics package smartdenovo<br>
Ultra-fast de novo assembler using long noisy reads
https://github.com/ruanjue/smartdenovo
smartdenovo Version: 1.0.0<br>

Singularity container based on the recipe: Singularity.smartdenovo_v1.0.0

Package installation using Miniconda3 V4.7.12<br>

image singularity (V>=3.3) is automaticly built (gitlab-ci) and pushed on the registry using the .gitlab-ci.yml <br>

can be pull (singularity version >=3.3) with:<br>
singularity pull smartdenovo_v1.0.0.sif oras://gitlab-registry.in2p3.fr/gafl1/singularity/smartdenovo_v1.0.0/smartdenovo_v1.0.0:latest


